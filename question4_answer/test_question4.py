import unittest

from question4_answer import question4
from unittest.mock import patch, call


class TestQuestion4(unittest.TestCase):

    @patch('builtins.print')
    def test_prime_int(self,mocked_print):
        # testing prime output with input int, the idea is compared the print output in terminal with provided result.
        run_cmd = question4.RunCommand()
        run_cmd.run(question4.GetFirstNumPrime(4))
        run_cmd.execute_command()
        assert mocked_print.mock_calls == [call("2, 3, 5, 7")]

    def test_prime_float(self):
        run_cmd = question4.RunCommand()
        # testing prime output with input float and should return TypeError
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(question4.GetFirstNumPrime(4.2)))


    def test_prime_str(self):
        run_cmd = question4.RunCommand()
        # testing prime output with input str and should return TypeError
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(question4.GetFirstNumPrime("4.2")))

    def test_prime_none(self):
        run_cmd = question4.RunCommand()
        # testing prime outputwith input none and should return TypeError
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(question4.GetFirstNumPrime(None)))

    def test_prime_list(self):
        # testing prime output with input list and should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(question4.GetFirstNumPrime([1])))

    def test_prime_dict(self):
        # testing prime output with input dict and should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(question4.GetFirstNumPrime({})))

    @patch('builtins.print')
    def test_combine(self,mocked_print):
        # testing combine list output with input 2 list, the idea is compared the print output in terminal
        # with provided result.
        run_cmd = question4.RunCommand()
        run_cmd.run(question4.CombineList([4, 3, 6, 5, 1, 2], ["F", "C", "D", "B", "A"]))
        run_cmd.execute_command()
        assert mocked_print.mock_calls == [call("[1:A],[2:B],[3:C],[4:D],[5:F],[6:NULL]")]

    def test_combine_none(self):
        # testing combine list output with input 1 list and 1 None. should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(run_cmd.run(
            question4.CombineList(None, ["F", "C", "D", "B", "A"]))))

    def test_combine_none_all(self):
        # testing combine list output with input 2 None. should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(run_cmd.run(
            question4.CombineList(None, None))))

    def test_combine_str(self):
        # testing combine list output with input 1 list and 1 str. should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(run_cmd.run(
            question4.CombineList("input", ["F", "C", "D", "B", "A"]))))

    def test_combine_int(self):
        # testing combine list output with input 1 list and 1 int. should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(run_cmd.run(
            question4.CombineList(1, ["F", "C", "D", "B", "A"]))))

    def test_combine_float(self):
        # testing combine list output with input 1 list and 1 float. should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(run_cmd.run(
            question4.CombineList(1.2, ["F", "C", "D", "B", "A"]))))

    def test_combine_dict(self):
        # testing combine list output with input 1 list and 1 dict. should return TypeError
        run_cmd = question4.RunCommand()
        with self.assertRaises(TypeError): run_cmd.execute_command(run_cmd.run(run_cmd.run(
            question4.CombineList({}, ["F", "C", "D", "B", "A"]))))














