from abc import ABC, abstractmethod


class Command(ABC):
    # this is the main class abstract method
    @abstractmethod
    def execute(self) -> None:
        pass

class GetFirstNumPrime(Command):
    # this is the class to find the first n number. the idea is that the number is prime that natural number cannot
    # be divided with any prime natural number before that number and resulted with non natural number.
    # for example 5 is prime because 5 cannot be divided with 2 and 3, however 4 is not prime because 4 can be divided
    # with prime number 2.

    def __init__(self, number: int) -> None:
        if not isinstance(number, int):
            raise TypeError("input must be a int")

        self._number = number

    def execute(self) -> None:
        print("{}".format(self.get_num_prime())[:-1][1:])

    def prime(self, i: int, primes: set):
        # method to check if number is prime or not
        for prime in primes:
            if not (i == prime or i % prime):
                return False
        primes.add(i)
        return i

    def get_num_prime(self) -> None:
        # method to generate first n number
        primes = set([2])
        i, p = 2, 0
        while True:
            if self.prime(i, primes):
                p = p + 1
                if p == self._number:
                    return primes
            i = i + 1


class CombineList(Command):
    # this class is used to combine 2 list. the idea is that take the biggest list and pair for each element with
    # the same index
    def __init__(self, input_1: list, input_2: list) -> None:

        if not isinstance(input_1, list) or not isinstance(input_2, list) :
            raise TypeError("input must be a list")
        self._input_1 = input_1
        self._input_2 = input_2
        self._input_1.sort()
        self._input_2.sort()

    def get_element(self, i: int, list_element: list):

        try:
            return list_element[i]
        except Exception as e:
            return "NULL"

    def execute(self) -> None:

        max_len = max(len(self._input_1), len(self._input_2))

        str_out = ""
        for i in range(0, max_len):
            element_1 = self.get_element(i, self._input_1)
            element_2 = self.get_element(i, self._input_2)

            str_out = str_out + ("[{}:{}],".format(element_1, element_2))

        print(str_out[:-1])


class RunCommand:
    # this class to run the command pattern
    _run = None

    def run(self, command: Command):
        self._run = command

    def execute_command(self) -> None:

        if isinstance(self._run, Command):
            self._run.execute()

if __name__ == "__main__":
    # initialize the command
    run_cmd = RunCommand()
    # insert method get first n prime number to the command and execute the command
    run_cmd.run(GetFirstNumPrime(4))
    run_cmd.execute_command()
    # insert method combine list to the command and execute the command
    run_cmd.run(CombineList([4, 3, 6, 5, 1, 2], ["F", "C", "D", "B", "A"]))
    run_cmd.execute_command()
