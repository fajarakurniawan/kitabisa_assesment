from airflow import DAG
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from datetime import datetime, timedelta
import psycopg2
from psycopg2.extras import execute_values
import logging
import ast
from airflow.utils.trigger_rule import TriggerRule

default_args = {
    'email': ['fajarakurniawan@gmail.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
    # 'provide_context': True
}

def extract():
    # get the data and pass into xcom
    str_json = """[{
            "id": "0001",
            "type": "donut",
            "name": "Cake",
            "ppu": 0.55,
            "batters":
                {
                    "batter":
                        [
                            { "id": "1001", "type": "Regular" },
                            { "id": "1002", "type": "Chocolate" },
                            { "id": "1003", "type": "Blueberry" },
                            { "id": "1004", "type": "Devil's Food" }
                        ]
                },
            "topping":
                [
                    { "id": "5001", "type": "None" },
                    { "id": "5002", "type": "Glazed" },
                    { "id": "5005", "type": "Sugar" },
                    { "id": "5007", "type": "Powdered Sugar" },
                    { "id": "5006", "type": "Chocolate with Sprinkles" },
                    { "id": "5003", "type": "Chocolate" },
                    { "id": "5004", "type": "Maple" }
                ]
        },
        {
            "id": "0002",
            "type": "donut",
            "name": "Raised",
            "ppu": 0.55,
            "batters":
                {
                    "batter":
                        [
                            { "id": "1001", "type": "Regular" }
                        ]
                },
            "topping":
                [
                    { "id": "5001", "type": "None" },
                    { "id": "5002", "type": "Glazed" },
                    { "id": "5005", "type": "Sugar" },
                    { "id": "5003", "type": "Chocolate" },
                    { "id": "5004", "type": "Maple" }
                ]
        },
        {
            "id": "0003",
            "type": "donut",
            "name": "Old Fashioned",
            "ppu": 0.55,
            "batters":
                {
                    "batter":
                        [
                            { "id": "1001", "type": "Regular" },
                            { "id": "1002", "type": "Chocolate" }
                        ]
                },
            "topping":
                [
                    { "id": "5001", "type": "None" },
                    { "id": "5002", "type": "Glazed" },
                    { "id": "5003", "type": "Chocolate" },
                    { "id": "5004", "type": "Maple" }
                ]
        }]"""
    return str_json

def transform(**context):
    # get the str data convert it into list of dict and process it into full columnar and pass into xcom
    json = ast.literal_eval(context['task_instance'].xcom_pull(task_ids='extract_task'))
    result = []
    for element in json:
        batters = element['batters']['batter']
        for batter in batters:
            for topping in element['topping']:
                data = {'product_id': element['id'], 'product_type': element['type'], 'product_name': element['name'], 'product_ppu': element['ppu'],
                        'batter_id': batter['id'], "batter_type": batter['type'], 'topping_id': topping['id'],
                        'topping_type': topping['type']
                        }
                result.append(data)

    return result



def check_table_exist():
    # check if table product exist if not exists then go to create_task otherwise go to insert_task
    try:
        connection = psycopg2.connect(user="postgres",
                                      password="postgres",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="postgres")

        cursor = connection.cursor()
        cursor.execute("""SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 where TABLE_NAME = 'product'""")

        record = cursor.fetchone()
        if not record:
            logging.info("Table Not Found")
            raise Exception
    except (Exception, psycopg2.Error) as error:
        logging.info(error)
        raise Exception
    finally:
        if (connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def create_table():
    # create table product
    try:
        connection = psycopg2.connect(user="postgres",
                                      password="postgres",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="postgres")

        cursor = connection.cursor()
        cursor.execute("""create table product (
            uuid serial primary key not null,
            product_id varchar,
            product_type varchar,
            product_name varchar,
            product_ppu varchar,
            batter_id varchar,
            batter_type varchar,
            topping_id varchar,
            topping_type varchar
        )""")
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        logging.info(error)
        raise Exception
    finally:
        if (connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

def insert_row(**context):
    # insert data into postgres from transform task
    try:
        connection = psycopg2.connect(user="postgres",
                                      password="postgres",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="postgres")
        cursor = connection.cursor()
        data = context['task_instance'].xcom_pull(task_ids='transform_task')
        columns = data[0].keys()
        query = "INSERT INTO product ({}) VALUES %s".format(','.join(columns))
        values = [[value for value in project.values()] for project in data]
        execute_values(cursor, query, values)
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        logging.info(error)
        raise Exception
    finally:
        if (connection):
            cursor.close()
            connection.close()
        print("PostgreSQL connection is closed")


dag = DAG('etl_json_to_columnar', description='Simple etl json to posgresql',
          schedule_interval='@daily',
          start_date=datetime(2020, 7, 9))

t0 = PythonOperator(
    task_id='extract_task',
    python_callable=extract,
    dag=dag)

t1 = PythonOperator(
    task_id='transform_task',
    python_callable=transform,
    provide_context=True,
    dag=dag)

t2 = PythonOperator(
    task_id='check_task',
    python_callable=check_table_exist,
    dag=dag)

t3 = PythonOperator(
    task_id='create_task',
    python_callable=create_table,
    trigger_rule=TriggerRule.ONE_FAILED,
    dag=dag)

t4 = PythonOperator(
    task_id='insert_task',
    python_callable=insert_row,
    trigger_rule=TriggerRule.ONE_SUCCESS,
    provide_context=True,
    dag=dag)

t1.set_upstream(t0)
t2.set_upstream(t1)
t3.set_upstream(t2)
t4.set_upstream(t3)
t4.set_upstream(t2)



