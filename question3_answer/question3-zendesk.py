
import requests

# get url and the password
url = 'https://fajara.zendesk.com/api/v2/ticket_fields.json'
user = 'whitephoenom@gmail.com'
pwd = 'pwdfajara'

# use request to hit the API
response = requests.get(url, auth=(user, pwd))


if response.status_code != 200:
    # if response status 200 then quit
    print('Status:', response.status_code, 'Problem with the request. Exiting.')
    exit()
else:
    # get json response from AP
    data = response.json()
    # get ticket fields
    list_ticket = data['ticket_fields']
    # print the tickets
    for ticket in list_ticket:
        print(ticket)